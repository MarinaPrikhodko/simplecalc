package com.company;

import javax.swing.*;
import javax.swing.event.AncestorListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleCalculator extends JFrame {


    private JPanel panel = new JPanel(new GridLayout(5, 1));
    private static JTextField firstNum = new JTextField();
    private static JTextField actionChar = new JTextField();
    private static JTextField secondNum = new JTextField();
    private static JTextField result = new JTextField();
    private JButton countRez = new JButton("=");
    {
        countRez.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double first = Double.parseDouble(firstNum.getText());
                String action = SimpleCalculator.actionChar.getText();
                double second = Double.parseDouble(secondNum.getText());
                if(action!="+"||action!="-"||action!="/"||action!="*"){
                    result.setText("ERROR");
                }
                if ("+".equals(action)) {
                    result.setText((first + second) + "");

                }
                if ("-".equals(action)) {
                    result.setText((first - second) + "");
                }
                if ("*".equals(action)) {
                    result.setText((first * second) + "");
                }
                if ("/".equals(action)) {
                    result.setText((first / second) + "");
                    if (second == 0) {
                        result.setText("На ноль делить нельзя");
                    }
                }
                firstNum.setText("");
                actionChar.setText("");
                secondNum.setText("");
            }
        });
    }



    public SimpleCalculator() {
        setBounds(300, 300, 300, 300);
        setLayout(new BorderLayout());
        setVisible(true);
        add(panel, BorderLayout.CENTER);
        panel.add(firstNum);
        panel.add(actionChar);
        panel.add(secondNum);
        panel.add(countRez);
        panel.add(result);
    }
}












